@extends('app_layouts.app')
@section('content')
<div class="row">
	<div class="col-md-6" ng-app>
		{{5+8}}
		<form class="form-horizontal" name="clientForm" ng-submit="submitForm()" novalidate>


			<div class="form-group" ng-class="{ 'has-error' : clientForm.name.$invalid && !clientForm.name.$untouched  }" >
				<label class="control-label col-sm-2" for="name">Name:</label>
				<div class="col-sm-5">
					<input type="text" name="name" ng-model="name" class="form-control" id="name" placeholder="Enter name" required>					
				</div>
				<div class="col-sm-5">
					<p ng-show="clientForm.name.$invalid && clientForm.name.$touched" class="help-block">You name is required.</p>
				</div>
			</div>



			<div class="form-group">
				<label class="control-label col-sm-2" >Gender:</label>
				<div class="col-sm-5">
					<div class="radio pull-left">
						<label><input type="radio" name="optradio">Male </label>
					</div>
					<div class="radio pull-right">
						<label><input type="radio" name="optradio" checked="checked">Female </label>
					</div>
				</div>
			</div>


			
			<div class="form-group" ng-class="{ 'has-error' : clientForm.phone.$invalid && !clientForm.phone.$untouched }">
				<label class="control-label col-sm-2" for="phone">Phone:</label>
				<div class="col-sm-5">
					<input type="number" name="phone" ng-model="phone" class="form-control" id="phone" placeholder="Enter phone" required>
				</div>
				<div class="col-sm-5">					
					<p ng-show="clientForm.phone.$error.required && !clientForm.phone.$untouched" class="help-block">phone is requires.</p>
				</div>
			</div>

			

			<div class="form-group" ng-class="{ 'has-error' : clientForm.email.$invalid && !clientForm.name.$untouched }">
				<label class="control-label col-sm-2" for="email">Email:</label>
				<div class="col-sm-5">
					<input type="email" class="form-control" name="email" ng-model="email" id="email" placeholder="Enter email" required>					
				</div>
				<div class="col-sm-5">
					<p ng-show="clientForm.email.$error.required && !clientForm.email.$untouched " class="help-block">Email is required.</p>
					<p ng-show="clientForm.email.$invalid && !clientForm.email.$untouched" class="help-block">Email is invalid.</p>
				</div>
			</div>
			


			<div class="form-group" ng-class="{ 'has-error' : clientForm.address.$invalid && !clientForm.address.$untouched }">
				<label class="control-label col-sm-2" for="address">Address:</label>
				<div class="col-sm-5">
					<input type="text" name="address" ng-model="address" class="form-control" id="address" placeholder="Enter address" required>
				</div>
				<div class="col-sm-5">
					<p ng-show="clientForm.address.$error.required && !clientForm.address.$untouched " class="help-block">Email is required.</p>
					
				</div>
			</div>


			<div class="form-group" ng-class="{ 'has-error' : clientForm.nationality.$invalid && !clientForm.nationality.$untouched }">
				<label class="control-label col-sm-2" for="nationality">Nationality:</label>
				<div class="col-sm-5">
					<input type="text" name="nationality" ng-model="nationality" class="form-control" id="nationality" placeholder="Enter nationality" required>
				</div>
				<div class="col-sm-5">
					<p ng-show="clientForm.nationality.$error.required && !clientForm.nationality.$untouched " class="help-block">Nationality is required.</p>					
				</div>
			</div>


			<div class="form-group" ng-class="{ 'has-error' : clientForm.dob.$invalid && !clientForm.dob.$untouched }">
				<label class="control-label col-sm-2" for="dob">Date of birth:</label>
				<div class="col-sm-5">
					<input type="date" name="dob" ng-model="dob" class="form-control" id="dob" placeholder="Enter date of birth" required>
				</div>
				<div class="col-sm-5">
					<p ng-show="clientForm.dob.$error.required && !clientForm.dob.$untouched " class="help-block">Date of birth is required.</p>					
				</div>
			</div>


			<div class="form-group" ng-class="{ 'has-error' : clientForm.educational_background.$invalid && !clientForm.educational_background.$untouched }">
				<label class="control-label col-sm-2" for="educational_background">Educational Background:</label>
				<div class="col-sm-5">
					<input type="text" name="educational_background" ng-model="educational_background" class="form-control" id="educational_background" placeholder="Enter Educational Background">
				</div>
				<div class="col-sm-5">
					<p ng-show="clientForm.educational_background.$error.required && !clientForm.educational_background.$untouched " class="help-block">Date of birth is required.</p>					
				</div>
			</div>
			
			<div class="form-group">
				<label class="control-label col-sm-2" for="prefered_mode">Prefered mode of contact:</label>
				<div class="col-sm-5">
					<select class="form-control" id="prefered_mode">
						<option>None</option>
						<option>Email</option>
						<option>Phone</option>
						
					</select>
				</div>
			</div>
			
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
					<button type="submit" class="btn btn-default">Submit</button>
				</div>
			</div>
		</form>
	</div>
	<div class="col-md-6">
	</div>
</div>
@endsection